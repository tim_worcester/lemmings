# Imports
import networkx as nx
import matplotlib.pyplot as plt

# creating empty lists as global variables to be filled and used by various
# functions and methods
routers_links_list = []
router_list = []
link_list = []
router_objects = []
link_objects = []


def ReadFile(filename):
    # open file specified in P2_main file (thinking this will
    # need to eventually be specified at run time?)
    with open(filename) as file:
        for line in file:
            # strip off newlines, split at commas and add each line
            # to a list of combined routers and links
            routers_links_list.append(line.rstrip('\n').split(','))
    # separate out routers and links and add them to their respective
    # list
    for line in routers_links_list:
        for item in line:
            if item == "Router":
                router_list.append(line)
            elif item == "Link":
                link_list.append(line)


def CreateRouterObjects(router_list):
    # from the router_list created in the ReadFile function,
    # create an object of the Router class for each line
    for line in router_list:
        router_object = Router(
            # linking required attributes from the Router class to
            # relevant column of the router_list
            router_name=line[1],
            speed=int(line[2]),
            memory_size=int(line[3])
            )
        # add each newly created object to a list of router_objects
        router_objects.append(router_object)


def CreateLinkObjects(link_list):
    # same as for CreateRouterObjects - maybe look at combining into 
    # CreateObjects using if statements..?
    for line in link_list:
        link_object = Link(
            source=line[1],
            bandwidth=int(line[2]),
            destination=line[3]
            )
        link_objects.append(link_object)


def CreateGraph():
    # Create an empty DiGraph
    G = nx.DiGraph()
    # commented this out as was creating dupe nodes:
    # for obj in router_objects:  
    #     G.add_node(obj, router_name="", speed="", memory_size="")

    # iterate through list of link objects and add them as edges to the graph
    for obj in link_objects:
        G.add_edge(obj.source, obj.destination, bandwidth=obj.bandwidth)
    # print to check graph created - turn into pytest
    print(nx.info(G))

    nx.draw_spectral(G)

    plt.show()


class Router():
    def __init__(self, router_name, speed, memory_size):
        self.router_name = router_name
        self.speed = speed
        self.memory_size = memory_size

    # Tim's class methods to go here


class Link():
    def __init__(self, source, bandwidth, destination):
        self.source = source
        self.bandwidth = bandwidth
        self.destination = destination
