# Imports
from P2_func_classes import *


if __name__ == "__main__":

    # define file to read network data from
    ReadFile("Routers_Links.txt")

    # print to check combined list created from file - turn into pytest
    print(routers_links_list)
    # print to check routers list only has routers in it - turn into pytest
    print(router_list)
    # print to check links list only has links in it - turn into pytest
    print(link_list)

    CreateRouterObjects(router_list)
    # print to check objects list created - turn this into pytest
    print(router_objects)

    CreateLinkObjects(link_list)
    # print to check objects list created - turn this into pytest
    print(link_objects)

    CreateGraph()


